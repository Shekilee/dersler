package ders18Enum;

public class Main {

    //how to declare enum variable
   static SizeShirt shirtSize = SizeShirt.XS;

    public static void main(String[] args) {
        //simple enum
        System.out.println(SizeShirt.XS);

        // to string @overrride
       System.out.println("HD size is :   " + DisplaySize.FullHD);

        //with switch enum
       System.out.println ("FullHD siz is : " + DisplaySizeSwitch.FullHD.getSize());

       //with enum Construktor
        System.out.println("HD size is : " + DisplaySizeConstruktor.FullHD);

    }

}
