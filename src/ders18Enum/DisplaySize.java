package ders18Enum;

public enum DisplaySize {
    SD{
        @Override
        public String toString (){

            return "480 P";
        }
    },
    HD{
        @Override
        public String toString(){
            return "720 P";
        }
    },
    FullHD(){
        @Override
        public String toString() {
            return "1080 P";
        }
    };

}
