package ders18Enum;

public enum DisplaySizeConstruktor {
    SD ("480 P"),
    HD ("720 P"),
    FullHD ("1080 P");

    private final String size ;
    private DisplaySizeConstruktor ( String size) {

        this.size = size;
    }
    public String getSize(){
        return size;

    }
}

