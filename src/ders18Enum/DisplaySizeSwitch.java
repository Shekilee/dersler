package ders18Enum;

public enum DisplaySizeSwitch {
    SD,
    HD,
    FullHD;
    public String getSize (){
        switch (this) {
            case SD :
                return "480 P";
            case HD:
                return "720 P";
            case FullHD:
                return "1080 P";
            default:
                return null;

        }
    }
}

