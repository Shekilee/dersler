package ders13Exception;

public class TryCatchHandling {
    static int a;
    static int b;
    static  int array [] = {1,2,3};
    public static void main (String [] args){
        try {
            //ashagida yazilanlardan ilki,birinci Exception olunur

            int c = a/b;
            int d = array[2];
        }catch (ArithmeticException e) {
            System.out.println("in class : " + e.getClass());
            System.out.println("error : " + e.getMessage());

        }catch (Exception e){

            //ikili "try " yazilanda bele yazilmasi meslehetdir

            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("Running");
        }
    }

}
