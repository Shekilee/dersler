package ders13Exception;

import java.util.Scanner;

public class ThrowSample {
    static int age;
    static String minimumAge = "17";
    static void enterAge () throws AgeVerifyException {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter age");
        age = sc.nextInt();
        if (age < 17)
            throw new AgeVerifyException("daxil olma yashi ashagidir : " + minimumAge);
    };



    public static void main(String[] args) {
        try {
            ThrowSample.enterAge();
    }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        }
}
