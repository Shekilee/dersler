package Ders22BitWaseCollectionsLambda;

import java.util.function.Consumer;

public class AnonymConsumer implements Consumer<String> {
    @Override
    public void accept(String s) {
        System.out.println(s);
    }
}
