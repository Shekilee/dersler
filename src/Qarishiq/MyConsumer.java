package Qarishiq;

import Qarishiq.Base;

public class MyConsumer extends Thread {

    private String name;

    public MyConsumer(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (true) {
            int lastIndex = Base.arrayList.size() - 1;
            if (lastIndex > -1) {
                String s = Base.arrayList.remove(lastIndex);
                System.out.println(name + " : " + s);
            }
        }

    }
}
