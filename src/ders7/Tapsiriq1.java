package ders7;

//        Metodlardan istifadə edərək kalkulyator yazin
//        Hər əməliyyatin öz metodu olsun.
//        Toplama , cixma,vurma,bölmə hamisi ayri -ayri metodlarda olsun

public class Tapsiriq1 {
}

class Calculator {

    //eded 1 = x       ;     eded2 = y

    public int toplama(int x, int y) {
        return x + y;
    }

    public int cixma(int x, int y) {
        return x - y;
    }

    public int vurma(int x, int y) {
        return x * y;
    }

    public int bolme(int x, int y) {
        if (y != 0) {
            return x / y;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
      int b=  calculator.bolme(4,2);
        System.out.println(b);
        int d= calculator.cixma(5,3);
        System.out.println(d);
        int c = calculator.vurma(23,7);
        System.out.println(c);
        int a = calculator.toplama(2,8);
        System.out.println(a);
    }
}