package ders7;

//Verilən stringin tersi ilə duzunun bir
// birnə bərabər olub olmamasini yoxlayin

import java.util.Scanner;

public class Tapsiriq2 {
    char [] reverseCharArray(char []array){
        char[] tersArray=new char[array.length];
        for (int i=0;i<array.length;i++){
            tersArray [i]=array[array.length-i-1];
        }
        return tersArray;
    }
    String reverseString (String Str) {
        char []inputChar=Str.toCharArray();
        char []reversedChars= reverseCharArray(inputChar);
        String reversedString=String.valueOf(reversedChars);
        return reversedString;
    }
    void compareString (){
        Scanner sc= new Scanner(System.in);
        System.out.println("yazini yaz");
        String str= sc.next();
        String reversedString = reverseString(str);
        if (str.equals(reversedString)){
            System.out.println("beraberdir");

        }else {
            System.out.println("beraber deyil");
        }
    }

    public static void main(String[] args) {
        Tapsiriq2 tapsiriq2= new Tapsiriq2();
        tapsiriq2.compareString();

    }
}
