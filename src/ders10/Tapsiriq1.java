package ders10;

import java.util.Scanner;

public class Tapsiriq1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ədədi qeyd edin");
        int eded = sc.nextInt();

        if (sade(eded)) {
            System.out.println(eded + " Sade ededdir");
        } else {
            System.out.println(eded + "Murekkeb ededdir");
        }
    }

    public static boolean sade (int eded) {
        if (eded < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(eded); i++) {
            if (eded % i == 0) {
                return false;
            }
        }
        return true;
    }
}
