package Ders2;//TAPSIRIQ 4
//
//        Klavaturtadan 3 eded daxil edirsen .a,b,c.
//        Eger (a+b)/c<10 capa verirsenki a ile b-nin ceminin c -ye bolunmesi 10-dan kicikdir ,
//        eger (a+b)/c=10  capa verirsenki a ile b-nin ceminin c -ye bolunmesi 10-a beraberdir ,
//        eger (a+b)/c>10  capa verirsenki a ile b-nin ceminin c -ye bolunmesi 10-dan boyukdur.
//        A=5
//        B=4
//        C=3
//        5 ile 4-un cemi 3-e bolunmesi 10 dan kicikdir.

public class EdedlerArasindaFerq {
    public static void main  (String[] args){
        int a=5;
        int b=4;
        int c=3;
        if (a+b/c<10) {
            System.out.println("kicikdir");
        } else if (a+b/c==10) {
            System.out.println("beraberdir");
        }else if (a+b/c>10){
            System.out.println("boyukdur");
        }
    }
}
