package ders20Collections;


import java.util.Arrays;

public class ArrayListDynamik {
    int size;
    int count = 0;

    int[] array;

    ArrayListDynamik(int size) {
        array = new int[size];

    }

    int size() {
        return array.length;
    }

    int get(int index) {
        return array[index];

    }
    int indexOf(int elem){
        for (int i=0; i < array.length ; i++){
            if(array[i]== elem){
                return i;
            }
        }
        return -1;

    }
    int remove(int index){
        int[] copyArray= new int [array.length];
        int removeElement = array[index];

        for (int i = 0;i <array.length ; i ++){
            if (i==index) continue;
            copyArray [i]= array[i];
        }
    array = copyArray;
        return removeElement;

    }



    void  add(int elem) {
        if (count + 1 == array.length){
            int [] copyArray = new int [array.length + array.length / 2];
            for (int i = 0 ; i < array.length ; i++){
                copyArray [i] = array[i];

            }
            array = copyArray;
        }


            array[count] = elem;
            count++;



    }
    @Override
    public String toString (){
        return Arrays.toString(array);


    }
}
