package TekrarBashtan;

import java.util.Scanner;

public class Tekrar1 {
    public static void main(String[] args) {
//        int a=8;
//        int b=7;
//        int c = 6;
//        System.out.println((a+b+c)/3);

        //       baslangic
//        System.out.println("Hello World");

        //data type
//        byte abc = 100;
//        System.out.println(abc);

        //      Char ve Boolean
//        char xarakter1 =117;
//        char xarakter2 = 'u';
//        System.out.println(xarakter1);
//        System.out.println(xarakter2);
//
//        boolean mentiq = true;
//        boolean mentiq2=false;
//        System.out.println(mentiq2);
//        System.out.println(mentiq);

        //String
//        char a = 'h';
//        String str = "hello";
//        System.out.println(str);
//        System.out.println(a);

        // Operatorlar
//        int a= 10;
//        int b= 5;
//        int toplama=a+b;
//        int vurma = a*b;
//        int cixma = a-b;
//        int bolme = a/b;
//        int kesr = a%b;
//        System.out.println("toplama: " + toplama);
//        System.out.println("Cixma :" + cixma);
//        System.out.println("vurma :" + vurma);
//        System.out.println("bolme: " + bolme);
//        System.out.println("kesr bolme: " +kesr);
//         toplama ++;
//        System.out.println(toplama);
//        cixma--;
//        System.out.println(cixma);
//
//        int netice = a++ + b--;
//        System.out.println(netice);
//        System.out.println(a);
//        System.out.println(b);

//        int eded1 = 10;
//        int eded2 = 5;
//
//        boolean shert = (eded1 / eded2)==2;
//        System.out.println(shert);
//
//        boolean shert1 = (eded1 >eded2);
//        boolean shert2 = (eded1 <eded2);
//        System.out.println(shert1);
//        System.out.println(shert2);
//
//        boolean netice = shert1 && shert2;
//        System.out.println(netice);
//
//        netice = shert1 || shert2;
//        System.out.println(netice);
//
//        String netice2 = (shert1 ) ? "dogru" :"sehv";
//        System.out.println(netice2);
//
//        eded1 = eded2;
//        System.out.println(eded1);
//        eded2 +=2;

        //Konsoldan melumat almaq

//        Scanner sc = new Scanner(System.in);
//        int a;
//        int b;
//        System.out.println("A ededinin daxil et");
//        a = sc.nextInt();
//        System.out.println("B ededini daxil et");
//        b= sc.nextInt();
//        System.out.println("A ededi :"  +  a);
//        System.out.println("B ededi :"  + b);
//         double c ;
//        System.out.println("Double novunde bir deyer daxil edin:");
//        c=sc.nextDouble();
//        System.out.println(c);
//            String str;
//        str= sc.nextLine();
//        System.out.println(str);

        //Algoritma
        //Telebenin orta qiymeti
        // ilk program
//        int quiz,vize,ffinal ;
//        double ortaQiymet ;
//        Scanner sc=new Scanner(System.in);
//
//        System.out.println("Quiz qiymetini daxil et");
//        quiz=sc.nextInt();
//        System.out.println("Vize qiymetini dacil et");
//        vize=sc.nextInt();
//        System.out.println("Ffinal qiymetini daxil et");
//        ffinal = sc.nextInt();
//        ortaQiymet = (quiz * 0.2) +(vize * 0.35) +(ffinal * 0.45);
//        System.out.println("orta qiymet :" + ortaQiymet );
//        String netice = (ortaQiymet >= 50) ? "kecdin" : "qaldin";
//        System.out.println(netice);


        //EDV hesablama
        // 1.mehsulun maya deyeri
        // 2.mehsulun edv-sini tapmaq
        // 3.edv faizi

//        double mayaDeyeri,edvQiymeti,edv=0.18;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("mehsulun mayaDeyerini daxil et");
//        mayaDeyeri = sc.nextDouble();
//        edvQiymeti = mayaDeyeri + (mayaDeyeri * edv);
//        System.out.println("mehsulun edv-li son qiymeti :" + edvQiymeti);


        //Dairenin sahesini ve cevrenin uzunlugu tapmaq
//        int r;
//        double sahe,cevreninUzunlugu,pi=3.14;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("dairenin radiusunu daxil et: \n" );
//        r = sc.nextInt();
//        cevreninUzunlugu = 2 * pi * r;           //cevrenin uzunlugu "l" ile isare olunur.
//        sahe = pi * (r*r);
//        System.out.println("dairenin sahesi : " + sahe);
//        System.out.println("cevrenin uzunlugu : " + cevreninUzunlugu);


        //Elaqelilik operatoru
//        int a = 10;
//        int b = 5;
//        boolean netice =(a==b);
//        System.out.println(netice);

        //Logical (mentiqi) operatorlar
//        int a=10;
//        int b = 20;
//        int c = 5;
//        int d = 50;
//
//        boolean shert1= a<b ;
//        boolean shert2 = c <d;
//        boolean netice = (shert1 && shert2);
//        boolean netice2= (a<b) && (d<c);
//        System.out.println(netice2);
//        System.out.println(netice);
//
//        boolean netice3= (shert1 || shert2);
//        System.out.println(netice3);

        //IF ve ELSE bloklari
//        int a =50;
//        int b = 30;
//        int c =30;
//        if (a==b){
//            System.out.println("a ve b edeleri beraberdir");
//        }else {
//            System.out.println("bereaber deyil");
//        }
//        if (b==c){
//            System.out.println("b ve c beraberdir");
//        }else if (a>c){
//            System.out.println("a c-den boyukdur");
//        }else{
//            System.out.println("b ve c beraber deyil");
//        }


//          //Switch case
//        Scanner sc = new Scanner(System.in);
//        System.out.println("eded daxil edin");
//        int eded = sc.nextInt();
//        switch (eded){
//            case 1:
//                System.out.println("eded 1-e beraberdir");
//                break;
//            case 2:
//                System.out.println("eded 2-e beraberdir");
//                break;
//            case 3:
//                System.out.println("eded 3-e beraberdir");
//                break;
//            default:
//                System.out.println("yararsiz");
//        }

//                // Switch case Kalkulyator
//        Scanner sc = new Scanner(System.in);
//        int eded1,eded2,secmek;
//        System.out.println("1ci ededi daxil edin");
//        eded1= sc.nextInt();
//        System.out.println("2ci ededi daxil edin");
//        eded2=sc.nextInt();
//        System.out.println("istediyiniz emeliyyati sec");
//        System.out.println("1- Toplama \n 2-Cixma \n 3-Vurma \n 4-bolme");
//        System.out.println("sechim: ");
//        secmek = sc.nextInt();
//        if (secmek == 1){
//            System.out.println("Toplama: " +(eded1 +eded2));
//        } else if (secmek==2) {
//            System.out.println("Cixma: " +(eded1-eded2));
//
//        } else if (secmek==3) {
//            System.out.println("Vurma: " +(eded1 * eded2));
//
//        }else if (secmek==4){
//            if (eded2==0)
//            System.out.println("emeliyyat uygunsuzdur");
//        }else {
//            System.out.println("bolme : " + (eded1/eded2));
//
//        } burada bashqa eded secende sistem xeta vermelidi,amma daxil etme qebul etmir

        //Switch case
        // Istifadeci girisi
//        Scanner sc = new Scanner(System.in);
//        String user,password;
//        System.out.println("Istifadeci adini daxil edin");
//        user = sc.nextLine();
//        System.out.println("parolu daxil edin");
//        password = sc.nextLine();
//        if (user.equals("java") && password.equals("123")){
//            System.out.println("ugurlu giris");
//        }else {
//            System.out.println("istifadeci adi yada parol sehvdir");
//
//        }
        // Gunun planlanmasi
        //Havanin tempraturu
        // istilik 30 derecden yuksek olarsa,denize getmek
        //10 ve 30 derece arasidirsa,kinoteatr-a getmek
        //10 dereceden asagidirsa,kitabxanaya
//        Scanner sc = new Scanner(System.in);
//        int istilik;
//        System.out.println("havanin tempraturunu daxil et");
//        istilik=sc.nextInt();
//        if (istilik > 30){
//            System.out.println("denize getmek olar");
//        }else if ((istilik>=10) && (istilik <30)) {
//            System.out.println("Kinoteatra getmek olar");
//        }else {
//            System.out.println("kitabxanaya getmek olar");
//
//        }

        //ededleri boyukden kiciye dogru yaz

        //a,b,c
        //a>b>c ve ya a>c>b
        // b>a>c> ve ya c>b>a
        //c>a>b> ve ya c>b>a
//        int a,b,c;
//        Scanner sc =new Scanner(System.in);
//        System.out.println("edeleri daxiledin");
//        a=sc.nextInt();
//        b=sc.nextInt();
//        c=sc.nextInt();
//        if ((a>b) && (a>c)){
//            if(b>c){
//                System.out.println("a>b>c");
//            }else {
//                System.out.println("a>c>b");
//            }
//        } else if (b>a && b>c) {
//            if (a > c) {
//                System.out.println("b>a>c");
//
//            }else {
//                System.out.println("b>c>a");
//            }
//        }else {
//            if (a>b ){
//                System.out.println("c>a>b");
//            }else {
//                System.out.println("c>b>a");
//            }
//        }











    }
}


